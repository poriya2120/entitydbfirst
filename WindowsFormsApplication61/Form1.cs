﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication61
{
    public partial class Form1 : Form
    {
        public void getlist()
        {
            DbTestEntities1 contex = new DbTestEntities1();
            var quey = from row in contex.sudent
                       select new
                       {
                           row.StudentId,
                           row.StudentCode,
                           row.StudentName,
                           row.StudentLastName,
                           row.studentAge
                       };
            dataGridView1.DataSource = quey.ToList();
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            getlist();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            sudent st = new sudent();
            st.StudentCode = Convert.ToInt64(textBox1.Text);
            st.StudentName = textBox2.Text;
            st.StudentLastName = textBox3.Text;
            st.studentAge = Convert.ToByte(textBox4.Text);
           DbTestEntities1 context = new DbTestEntities1();
            context.sudent.Add(st);
            context.SaveChanges();
            getlist();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Tag = dataGridView1.CurrentRow.Cells[0].Value.ToString();

            textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();

            textBox3.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();

            textBox4.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DbTestEntities1 context = new DbTestEntities1();
            long id = Convert.ToInt16(textBox1.Tag.ToString());
            sudent st = context.sudent
                .Where(c => c.StudentId == id).First();
            st.StudentCode = Convert.ToInt64(textBox1.Text);
            st.StudentName = textBox2.Text;
            st.StudentLastName = textBox3.Text;
            st.studentAge= Convert.ToByte(textBox4.Text);
            context.SaveChanges();
            getlist();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            long id = Convert.ToInt16(textBox1.Tag.ToString());
            DbTestEntities1 context = new DbTestEntities1();
            sudent st = context.sudent
                .Where(c => c.StudentId ==id).First();
            context.sudent.Remove(st);
            context.SaveChanges();
            getlist();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            DbTestEntities1 contex = new DbTestEntities1();
            var quey = from row in contex.sudent
                       where row.StudentName.StartsWith(textBox2.Text)
                       select row;
            dataGridView1.DataSource = quey.ToList();
                       
        }
    }
}
